import { Link, useNavigate } from 'react-router-dom';
import storage from '../Storage/storage';

const Nav = () => {
  const centerText = {
    display: 'flex',
    justifyContent: 'center',
  };

  return (
    <nav className='navbar navbar-expand-lg navbar-white bg-warning'>
      <div className='container-fluid'>
        <a className='navbar-brand' style={centerText}>API MARVEL</a>
        <button className='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#nav' aria-controls='navbarSupportedContent'>
          <span className='navbar-toggler-icon'></span>
        </button>
      </div>
    </nav>
  );
};

export default Nav;
